import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Login } from 'src/app/interfaces/login';
import { ErrorMsgComponent } from 'src/app/compartilhado/error-msg/error-msg.component';
import { HttpResponse, HttpHeaderResponse, HttpEvent, HttpHandler } from '@angular/common/http';
import { JwtResponse } from 'src/app/interfaces/jwt-response';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  imagemSrc = 'assets/img/img-03.png';
  @ViewChild(ErrorMsgComponent) errorMsgComponent: ErrorMsgComponent;
  constructor(private usuarioService: UsuarioService, private router: Router) { }
  login: Login = <Login>{};

  onSubmit() {
    this.usuarioService.autenticacao(this.login)
    .subscribe((jwtResponse) => {
      localStorage.setItem('token', jwtResponse.token);
      if(jwtResponse.perfil == "CLIENTE") { this.router.navigateByUrl(`/home/${this.login.email}`); }
      else { this.router.navigateByUrl(`/admin/${this.login.email}`); }
    },
      () => {
      this.errorMsgComponent.setError('Falha ao autenticar');
      });
  }

}
