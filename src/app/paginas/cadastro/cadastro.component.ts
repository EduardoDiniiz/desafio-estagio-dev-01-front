import { Component, ViewChild, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
import { ErrorMsgComponent } from 'src/app/compartilhado/error-msg/error-msg.component';
import { Usuario } from 'src/app/interfaces/usuario';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit{
  formularioDeUsuario: FormGroup;
  @ViewChild(ErrorMsgComponent) errorMsgComponent: ErrorMsgComponent;
  constructor(private usuarioService: UsuarioService, private fb: FormBuilder, private router: Router) {
  }
  usuario: Usuario = <Usuario>{};
  ngOnInit(): void {
    this.criarFormularioDeUsuario();
}

  onSubmit() {
    if (this.formularioDeUsuario.valid) {
      this.usuarioService.addUsuario(this.usuario).subscribe(() => {
// tslint:disable-next-line: triple-equals
        if(this.usuario.tipoUsuario == 2) { this.router.navigateByUrl(`/home/${this.usuario.email}`); }
        else { this.router.navigateByUrl(`/admin/${this.usuario.email}`); }
      },
        () => {
        this.errorMsgComponent.setError('Não foi possível completar cadastro');
        });
// tslint:disable-next-line: max-line-length
    } else { this.errorMsgComponent.setError('Todos os campos são obrigatorios. A Senha deve conter no minímo 1 letra maiúscula e minúscula e números.'); }
  }

  criarFormularioDeUsuario() {
    this.formularioDeUsuario = this.fb.group({
      nome: ['', Validators.compose([ Validators.required, Validators.minLength(3), Validators.max(60) ])],
      email: ['', Validators.compose([Validators.required])],
// tslint:disable-next-line: max-line-length
      senha: ['', Validators.compose([Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=^.{8,50}$).*$')])],
      tipo: ['', Validators.compose([Validators.required])]
    });
}
}
