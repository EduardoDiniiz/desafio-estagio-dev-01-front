import { Component, ViewChild } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Usuario } from 'src/app/interfaces/usuario';
import { ErrorMsgComponent } from '../../compartilhado/error-msg/error-msg.component';
import { HttpResponse, HttpHandler, HttpHeaderResponse } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  public token: string;
  public usuario: Usuario;
  @ViewChild(ErrorMsgComponent) errorMsgComponent: ErrorMsgComponent;
  constructor(private usuarioService: UsuarioService, private activatedRouter: ActivatedRoute, private router: Router) {
    this.getUsuario(this.activatedRouter.snapshot.params.email);
    this.token = localStorage.getItem('token');
   }

  getUsuario(email: string){
    this.usuarioService.getUsuarioByEmail(email)
      .subscribe((usuario: Usuario) => {
        this.usuario = usuario;
      }, () => {
        this.errorMsgComponent.setError('Não foi possível buscar usuario');
      });
  }

}
