import { Component, ViewChild } from '@angular/core';
import { Usuario } from 'src/app/interfaces/usuario';
import { ErrorMsgComponent } from 'src/app/compartilhado/error-msg/error-msg.component';
import { ActivatedRoute } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent {
  public token: string;
  public usuario: Usuario;
  @ViewChild(ErrorMsgComponent) errorMsgComponent: ErrorMsgComponent;
  constructor(private usuarioService: UsuarioService, private activatedRouter: ActivatedRoute) {
    this.getUsuario(this.activatedRouter.snapshot.params.email);
    this.token = localStorage.getItem('token');
   }

  getUsuario(email: string){
    this.usuarioService.getUsuarioByEmail(email)
      .subscribe((usuario: Usuario) => {
        this.usuario = usuario;
      }, () => {
        this.errorMsgComponent.setError('Não foi possível buscar usuario');
      });
  }
}
