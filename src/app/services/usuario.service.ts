import { Injectable } from '@angular/core';
import { Usuario } from 'src/app/interfaces/usuario';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpEvent, HttpResponse, HttpHeaderResponse, HttpHandler, HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Login } from 'src/app/interfaces/login';
import { map } from 'rxjs/operators';
import { JwtResponse } from '../interfaces/jwt-response';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {


  constructor(private http: HttpClient ) {

  }

  addUsuario(usuario: Usuario): Observable<Usuario> {
    const url = `${environment.loginApiUrl}/usuario`;
    return this.http.post<Usuario>(url, usuario);
  }

  autenticacao(login: Login): Observable<JwtResponse>{
    const url = `${environment.loginApiUrl}/usuario/login`;
    return this.http.post<JwtResponse>(url, login);
  }

  getUsuarioByEmail(email: string): Observable<Usuario> {
    const url = `${environment.loginApiUrl}/usuario/email/${email}`;
    return this.http.get<Usuario>(url);
  }
}